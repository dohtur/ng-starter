import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'input-box',
    template: `<input type="text" placeholder="{{text}}" #box (keydown.enter)=onAdd(box)> <button (click)="clear(box)">Clear</button>`,
    styles: [`
        input, button {
            padding: 5px 7px;
            border-radius: 3px;
            border: 1px solid #e2e2e2;
            font-size: 14px;
        }
        button {
            background-color: #205081;
            color: #fff;
        }
    `]
})
export class InputBox {
    @Input()
    text: string;

    @Output()
    addText = new EventEmitter();

    onAdd(box) {
        this.addText.emit(box.value);
        this.clear(box);
    }

    clear(box) {
        box.value = '';
    }
}