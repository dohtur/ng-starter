import { Component, Input } from '@angular/core';
import { NgIf, NgFor } from '@angular/common';

import { Text } from './text.component';
import { InputBox } from './input.component';

@Component({
    selector: 'my-app',
    directives: [Text, InputBox],
    template: `<h1>MyApp with wp</h1>
                <input-box (addText)="onAdd($event)" text="Write something"></input-box>
                <ul>
                    <li *ngFor="let item of items; let idx=index">{{ item }} 
                        <button (click)="delItem(idx)"><i class="fa fa-fw fa-trash"></i></button>
                    </li>
                </ul>
                <text></text>`,
    styles: [`
        h1 {
            color: #333;
        }
        ul {
            list-style: none;
            margin: 10px;
            padding: 0 10px;
        }
        li {
            margin-bottom: 5px;
        }
        button {
            background-color: #f00;
            color: #fff;
            border-radius: 2px;
            padding: 2px;
            border: 1px solid #ab2e2e;
        }
    `] 
})

export class AppComponent { 
    @Input()
    items: Array<string> = [];

    onAdd(text){
        this.items.push(text);
        
    }

    delItem(idx){
        let newItems = this.items.filter((item, i)=>{
                            return idx !== i;
                        }); 
        this.items = newItems;
    }
}
