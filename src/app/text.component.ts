import { Component } from '@angular/core';

@Component({
    selector: 'text',
    template: '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit aspernatur vel officiis iste, doloremque consectetur necessitatibus, accusantium tenetur. Quaerat expedita veniam nesciunt at unde animi. Consectetur minima, voluptates assumenda et!</p>',
    styles: [`
        p {
            width: 30%;
            margin: 10px 0;
            padding-left: 5px;
            border-left: 2px solid #e2e2e2;
        }
    `]

})

export class Text { }
